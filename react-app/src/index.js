import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import AppNavBar from './AppNavBar'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


// const name = 'John Smith';
/*const user = {
  firstName: 'Jane',
  lastName: 'Smith',
}

function formatName(user) {
  return user.firstName + ' ' + user.lastName
}

const element = <h1> Hello, {formatName(user)}</h1>

ReactDOM.render(
  element,
  document.getElementbyId('root')
);
*/