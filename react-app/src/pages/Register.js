import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register (){

	const navigate = useNavigate();
	const { user } = useContext(UserContext)
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);


	//console.log(email);
	//console.log(password);
	//console.log(password2);

	function registerUser(e) {

		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })

        .then(res => res.json())
        .then(data => {
            console.log(`Data: ${data}`)
            if(data === false){
                register()
            } else{
                Swal.fire({
                    title: 'Duplicate email found',
                    text: 'Please provide a different email',
                    icon: 'error'
                })
                setFirstName(firstName);
                setLastName(lastName);
                setEmail(email);
                setMobileNo(mobileNo);
                setPassword('');
                setPassword2('');
            }
        })
    }


    const register = () => {

    	fetch('http://localhost:4000/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
                },
            body: JSON.stringify({
            	firstName: firstName,
            	lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
                })
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: 'Registration Successful',
					text: 'Welcome to Zuitt!',
					icon: 'success'
				})

				navigate("/login")
			}

		})
	}


	useEffect(() => {

		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password !== '' ) && (mobileNo.length === 11) && (password === password2)) {
			setIsActive(true);

		} else {
			
			setIsActive(false)
		}

	}, [firstName, lastName, mobileNo, email, password, password2])

	const storage = localStorage.getItem('token')

	return(
		(storage !== null) ? 

		<Navigate to="/courses"/>
			
			:
		<Container>
			<Form onSubmit = {(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter first name"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
     
                        required
                    />
            </Form.Group>
			<Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter last name"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
     
                        required
                    />
              </Form.Group>
			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    				type="email" 
			    				placeholder="Enter email"
			    				value = {email}
			    				onChange = {e => setEmail(e.target.value)}
			    				required />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>
			  <Form.Group controlId="mobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="Enter mobile number"
                        value={mobileNo}
                        onChange={ e => setMobileNo(e.target.value)}
                        required />
              </Form.Group>
			  <Form.Group className="mb-3" controlId="password">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    				type="password" 
			    				placeholder="Password"
			    				value={password}
			    				onChange= {e => setPassword(e.target.value)}
			    				required />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label> Verify Password</Form.Label>
			    <Form.Control 
			    				type="password" 
			    				placeholder="Verify Password"
			    				value={password2}
			    				onChange={e=> setPassword2(e.target.value)}
			    				required />
			  </Form.Group>

			  {
			  	isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  		:
			  		<Button variant="danger" type="submit" id="submitBtn" disabled>
			  		  Submit
			  		</Button>
			  }

			</Form>
		</Container>
		)

}

/*

	Mini-Activity:
		If the user goes to /register route (Registration Page) when the user is already logged in, navigate/redirect to the /courses route (Courses Page)

		6:30 PM

*/
